#!/bin/bash

# Set the database connection variables
source .env_postgres

export PGPASSWORD=$mypassword

# Check for schemas
schemas=$(psql -h $host -p $port -U $user -d $dbname -c "\dn;")
echo "Schemas in $dbname: $schemas"
# Check for tables
tables=$(psql -h $host -p $port -U $user -d $dbname -c "\dt;")
echo "Tables in $dbname: $tables"

# ENV File
Environment file should be named as .env_postgres  
.env_postgres file needs to contain database connection information shown in .env_example

# BUILD DOCKER IMAGE
docker build . -t $image_name
# RUN DOCKER CONTAINER
docker run $image_name

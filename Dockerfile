FROM postgres:latest

COPY script.sh .env_postgres /

RUN chmod +x script.sh

CMD ["/script.sh"]
